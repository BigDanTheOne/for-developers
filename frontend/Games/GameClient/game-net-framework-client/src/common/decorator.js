export function event_(eventType, allow_overwriting = false) {
    return function (target, propertyKey, descriptor) {
        console.log("!!!!!!!!!!!!!!!!!", target, propertyKey, descriptor);
        if (!target.__events__) {
            console.log("!!!!!!!!!!!!!!!!!");
            target.__events__ = new Map();
        }
        if (eventType < 0) {
            throw `Event type must be positive`;
        }
        if (target.__events__.get(eventType) === undefined) {
            console.log(1);
            target.__events__.set(eventType, descriptor.value);
        }
        else if (allow_overwriting == true) {
            console.log(2);
            target.__events__.set(eventType, descriptor.value);
        }
        else {
            throw `overwriting event type ${eventType} handler is forbidden
            (set allow_overwriting = true if you want to overwrite event type handler)`;
        }
        return descriptor;
    };
}
