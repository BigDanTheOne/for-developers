import {EventHandler, EventType} from "./events.js";
import {BClient} from "../game-client/client.js";

export function event_(eventType: EventType, allow_overwriting : boolean = false) : any {
    return function (target: BClient, propertyKey: string, descriptor: PropertyDescriptor)
        : PropertyDescriptor {
        console.log("!!!!!!!!!!!!!!!!!", target, propertyKey, descriptor)
        if (!target.__events__) {
            console.log("!!!!!!!!!!!!!!!!!")
            target.__events__ = new Map<EventType, EventHandler>();
        }
        if (eventType < 0) {
            throw `Event type must be positive`
        }
        if (target.__events__.get(eventType) === undefined) {
            console.log(1)
            target.__events__.set(eventType, descriptor.value);
        }
        else if (allow_overwriting == true) {
            console.log(2)
            target.__events__.set(eventType, descriptor.value);
        }
        else {
            throw `overwriting event type ${eventType} handler is forbidden
            (set allow_overwriting = true if you want to overwrite event type handler)`
        }
        return descriptor;
    }
}