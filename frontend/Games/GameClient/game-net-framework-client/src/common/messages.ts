import { User } from './user.js';
import {Player} from "./events";


export enum MessageType {
    UserJoinedMessage ,
    PartyBeginsMessage ,
    GameBeginsMassage, //TODO: correct
    GameEndsMessage ,
    PartyEndsMessage ,
    GameEventMessage,
    RequestNewGameSession,
    RequestNewRoomMessage,
    JoinRoomMessage,
    NewRoomMessage,
    PlayerReadinessMessage,
};

export type IDType = string;
export class Message {
    MessageType : MessageType;
    constructor(msgType: MessageType) {
        this.MessageType = msgType;
    }
}
interface IBaseOptions {
    MessageType : MessageType;
    Users? : [User]
    GamePath? : string;
    Scores? : Map<IDType, number>
    User? : string;
    RoomCode? : string;
    Ready? : boolean
}

export class BaseMessage extends Message {
    Users? : [User]
    GamePath? : string;
    Scores? : Map<IDType, number>
    User? : string;
    RoomCode? : string;
    Ready? : boolean

    constructor( opts : IBaseOptions) {
        super(opts.MessageType)
        console.log(opts)
        if (opts.Users) {
            this.Users = opts.Users
        }
        if (opts.GamePath != undefined) {
            this.GamePath = opts.GamePath
        }
        if (opts.Scores != undefined) {
            this.Scores = opts.Scores
        }
        if (opts.User) {
            this.User = opts.User
        }
        if (opts.RoomCode != undefined) {
            this.RoomCode = opts.RoomCode
        }
        if (opts.Ready != undefined) {
            this.Ready = opts.Ready
        }

    }

    public Serialize () : string {
        return JSON.stringify(this);
    }
}

export function Deserialize (json :string) : BaseMessage {
    let msg
    let parsed_msg = JSON.parse(json)
    console.log("parsed msg: ", parsed_msg)
    switch (parseInt(parsed_msg.MessageType)) {
        case MessageType.UserJoinedMessage:
            msg = new BaseMessage({MessageType : MessageType.UserJoinedMessage, Users: parsed_msg.Users})
            break
        case MessageType.PartyBeginsMessage:
            msg = new BaseMessage({MessageType : MessageType.PartyBeginsMessage})
            break
        case MessageType.GameBeginsMassage:
            msg = new BaseMessage({MessageType : MessageType.GameBeginsMassage,GamePath : parsed_msg.GamePath})
            break
        case MessageType.GameEndsMessage:
            msg = new BaseMessage({MessageType : MessageType.GameEndsMessage, Scores : parsed_msg.Scores})
            break
        case MessageType.PartyEndsMessage:
            msg = new BaseMessage({MessageType : MessageType.PartyEndsMessage, Scores : parsed_msg.Score})
            break
        case MessageType.NewRoomMessage:
            console.log(6)
            msg = new BaseMessage({MessageType : MessageType.NewRoomMessage, RoomCode : parsed_msg.RoomCode})
            break
        case MessageType.GameEventMessage:
            msg = new GameEventsMessage(parsed_msg.Id, parsed_msg.Body, parsed_msg.EventType)
            console.log(parsed_msg.EventType)
            console.log('!@#$%^&*(')
            console.log(msg)
            break

    }
    return msg
}

export class GameEventsMessage extends Message{
    Id: IDType
    Body: any
    EventType: any
    Players? : (Player)[]
    constructor(id : IDType, body: any, event: any, Players?: (Player)[]) {
        super(MessageType.GameEventMessage)
        this.Body = body
        this.EventType = event
        this.Id = id
        if (Players != undefined) {
            this.Players = Players
        }
    }

    public Serialize () : string {
        return JSON.stringify(this);
    }
}

export class MessagesFactory{
    counter : number
    constructor() {
        this.counter = 0;
    }
    public MakeMessage(Body : any, EventType : any) : BaseMessage {
        // implement more complex logic of index incrementing
        //add assertions (may be they are included in typescript compiler)
            return new GameEventsMessage(String(this.counter++), Body, EventType)
    }
}
