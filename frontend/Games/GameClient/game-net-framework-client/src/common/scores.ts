import { UserID } from "./user.js";

export type Scores = Map<UserID, number>;