export var MessageType;
(function (MessageType) {
    MessageType[MessageType["UserJoinedMessage"] = 0] = "UserJoinedMessage";
    MessageType[MessageType["PartyBeginsMessage"] = 1] = "PartyBeginsMessage";
    MessageType[MessageType["GameBeginsMassage"] = 2] = "GameBeginsMassage";
    MessageType[MessageType["GameEndsMessage"] = 3] = "GameEndsMessage";
    MessageType[MessageType["PartyEndsMessage"] = 4] = "PartyEndsMessage";
    MessageType[MessageType["GameEventMessage"] = 5] = "GameEventMessage";
    MessageType[MessageType["RequestNewGameSession"] = 6] = "RequestNewGameSession";
    MessageType[MessageType["RequestNewRoomMessage"] = 7] = "RequestNewRoomMessage";
    MessageType[MessageType["JoinRoomMessage"] = 8] = "JoinRoomMessage";
    MessageType[MessageType["NewRoomMessage"] = 9] = "NewRoomMessage";
    MessageType[MessageType["PlayerReadinessMessage"] = 10] = "PlayerReadinessMessage";
})(MessageType || (MessageType = {}));
;
export class Message {
    constructor(msgType) {
        this.MessageType = msgType;
    }
}
export class BaseMessage extends Message {
    constructor(opts) {
        super(opts.MessageType);
        console.log(opts);
        if (opts.Users) {
            this.Users = opts.Users;
        }
        if (opts.GamePath != undefined) {
            this.GamePath = opts.GamePath;
        }
        if (opts.Scores != undefined) {
            this.Scores = opts.Scores;
        }
        if (opts.User) {
            this.User = opts.User;
        }
        if (opts.RoomCode != undefined) {
            this.RoomCode = opts.RoomCode;
        }
        if (opts.Ready != undefined) {
            this.Ready = opts.Ready;
        }
    }
    Serialize() {
        return JSON.stringify(this);
    }
}
export function Deserialize(json) {
    let msg;
    let parsed_msg = JSON.parse(json);
    console.log("parsed msg: ", parsed_msg);
    switch (parseInt(parsed_msg.MessageType)) {
        case MessageType.UserJoinedMessage:
            msg = new BaseMessage({ MessageType: MessageType.UserJoinedMessage, Users: parsed_msg.Users });
            break;
        case MessageType.PartyBeginsMessage:
            msg = new BaseMessage({ MessageType: MessageType.PartyBeginsMessage });
            break;
        case MessageType.GameBeginsMassage:
            msg = new BaseMessage({ MessageType: MessageType.GameBeginsMassage, GamePath: parsed_msg.GamePath });
            break;
        case MessageType.GameEndsMessage:
            msg = new BaseMessage({ MessageType: MessageType.GameEndsMessage, Scores: parsed_msg.Scores });
            break;
        case MessageType.PartyEndsMessage:
            msg = new BaseMessage({ MessageType: MessageType.PartyEndsMessage, Scores: parsed_msg.Score });
            break;
        case MessageType.NewRoomMessage:
            console.log(6);
            msg = new BaseMessage({ MessageType: MessageType.NewRoomMessage, RoomCode: parsed_msg.RoomCode });
            break;
        case MessageType.GameEventMessage:
            msg = new GameEventsMessage(parsed_msg.Id, parsed_msg.Body, parsed_msg.EventType);
            console.log(parsed_msg.EventType);
            console.log('!@#$%^&*(');
            console.log(msg);
            break;
    }
    return msg;
}
export class GameEventsMessage extends Message {
    constructor(id, body, event, Players) {
        super(MessageType.GameEventMessage);
        this.Body = body;
        this.EventType = event;
        this.Id = id;
        if (Players != undefined) {
            this.Players = Players;
        }
    }
    Serialize() {
        return JSON.stringify(this);
    }
}
export class MessagesFactory {
    constructor() {
        this.counter = 0;
    }
    MakeMessage(Body, EventType) {
        // implement more complex logic of index incrementing
        //add assertions (may be they are included in typescript compiler)
        return new GameEventsMessage(String(this.counter++), Body, EventType);
    }
}
