import {IGameEvent, IGameMessage} from "./events.js";
import {Scores} from "./scores.js";

export interface eventReceiver {
    (e: IGameEvent): void;
}

export interface gameCloser {
    (s: Scores): void;
}