import {MessageType} from "./messages.js";

export type EventType = number;

export class Player {
    Id: string;
    Name: String;
}

export interface IGameEvent {
    EventType: EventType;
    Players? : [Player];
    Broadcast?: boolean;
    Body?: string;

}

export class IGameMessage {
    EventType: EventType;
    Broadcast?: boolean;
    Body?: string;
    MessageType: number
}

export interface  EventHandler extends Function {
    (event: IGameEvent): void;
}
export function MakeIGameMessage(evnt: IGameEvent) : IGameMessage {
    console.log(evnt)
    return {EventType : evnt.EventType, Broadcast : evnt.Broadcast, Body : evnt.Body,
        MessageType : MessageType.GameEventMessage}
}