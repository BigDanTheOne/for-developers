import { MessageType } from "./messages.js";
export class Player {
}
export class IGameMessage {
}
export function MakeIGameMessage(evnt) {
    console.log(evnt);
    return { EventType: evnt.EventType, Broadcast: evnt.Broadcast, Body: evnt.Body,
        MessageType: MessageType.GameEventMessage };
}
