import { MakeIGameMessage } from '../common/events.js';
export class BClient {
    constructor(id) {
        //example: this.__events__[ForceExit] = force_exit_handler;
        this.__id__ = id;
        this.__events__.set(-1, (event) => { this.players = event.Players; });
    }
    SetWindow(window) {
        this.my_window = window;
        window.addEventListener("message", (event) => {
            console.log("received message from server:", event.data);
            this.trigger(event.data);
        }, false);
    }
    trigger(event) {
        console.log(this.__events__);
        if (this.__events__ === undefined) {
            throw 'No tagged handlers';
        }
        if (!this.__events__.has(event.EventType)) {
            throw `Event type ${event.EventType} not handled`;
        }
        this.__events__.get(event.EventType).apply(this, [event]);
    }
    /** end signals that the game has ended and returns scores */
    end(scores) {
        this.closer_cb(scores);
    }
    /** feeds server events to client */
    feed(event) {
        console.log(event);
        // TODO: figure out how to run in background
        this.event_cb(event);
    }
    on(receiver) {
        if (this.event_cb != undefined) {
            throw `Event handler already set: ${this.event_cb}`;
        }
        this.event_cb = (event) => {
            console.log("sending msg:", event);
            this.my_window.parent.postMessage(MakeIGameMessage(event), "*");
            receiver(event);
        };
    }
    onClose(closer) {
        if (this.closer_cb != undefined) {
            throw `Closer already set: ${this.closer_cb}`;
        }
        this.closer_cb = (event) => {
            this.my_window.parent.postMessage(MakeIGameMessage(event), "*");
            closer(event);
        };
    }
}
export function wireClient(eventCallback, closerCallback, type) {
    let client = new type("id123456");
    console.log("client: ", client);
    client.on(eventCallback);
    client.onClose(closerCallback);
    client.init();
    return client;
}
