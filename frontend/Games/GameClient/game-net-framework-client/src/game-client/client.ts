import {EventType, EventHandler, IGameEvent, MakeIGameMessage, Player} from '../common/events.js'
import {Scores} from '../common/scores.js'
import {eventReceiver, gameCloser} from '../common/callbackes.js'

export type ClientID = string;
export abstract class BClient {
    __id__: ClientID;
    __events__: Map<EventType, EventHandler>;
    players: [Player]

    // callbacks
    event_cb: eventReceiver;
    closer_cb: gameCloser;
    //my_document: any
    my_window: any

    constructor(id: ClientID) {
        //example: this.__events__[ForceExit] = force_exit_handler;
        this.__id__ = id;
        this.__events__.set(-1, (event) => {this.players = event.Players})
    }

    public SetWindow(window : any) {
        this.my_window = window;
        window.addEventListener("message", (event) => {
            console.log("received message from server:", event.data);
            this.trigger(event.data)
        }, false);
    }

    public trigger(event: IGameEvent): void {
        console.log(this.__events__)
        if (this.__events__ === undefined) {
            throw 'No tagged handlers';
        }

        if (!this.__events__.has(event.EventType)) {
            throw `Event type ${event.EventType} not handled`
        }

        this.__events__.get(event.EventType).apply(this, [event]);
    }

    /** init is called once the game starts */
    abstract init(): void;

    /** end signals that the game has ended and returns scores */
    end(scores: Scores): void {
        this.closer_cb(scores);
    }

    /** feeds server events to client */
    feed(event : IGameEvent): void {
        console.log(event);
        // TODO: figure out how to run in background
        this.event_cb(event)
    }

    on(receiver: eventReceiver) {
        if (this.event_cb != undefined) {
            throw `Event handler already set: ${this.event_cb}`;
        }
        this.event_cb = (event: any) => {
            console.log("sending msg:", event);
            this.my_window.parent.postMessage(MakeIGameMessage(event),"*");
            receiver(event)
        }
    }

    onClose(closer: gameCloser) {
        if (this.closer_cb != undefined) {
            throw `Closer already set: ${this.closer_cb}`;
        }
        this.closer_cb = (event: any) => {
            this.my_window.parent.postMessage(MakeIGameMessage(event),"*");
            closer(event)
        };
    }
}


export function wireClient<SS extends BClient>
(eventCallback: eventReceiver,
 closerCallback: gameCloser,
 type: {new(id : string): SS}): SS {
    let client = new type("id123456");
    console.log("client: ", client)
    client.on(eventCallback);
    client.onClose(closerCallback);

    client.init();

    return client;
}
