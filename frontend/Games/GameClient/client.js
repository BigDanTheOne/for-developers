var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { BClient } from './game-net-framework-client/src/game-client/client.js';
import { event_ } from './game-net-framework-client/src/common/decorator.js';
export class ChatClient extends BClient {
    constructor(id) {
        super(id);
    }
    show_msg(event) {
        let body = JSON.parse(event.Body);
        for (let id in body) {
            if (id == "me") {
                document.getElementById("log").value =
                    document.getElementById("log").value + '\n' + id + " : " + body[id];
            }
            else {
                let nickname;
                for (let player of this.players) {
                    if (player.Id == id) {
                        nickname = player.Name;
                        break;
                    }
                }
                document.getElementById("log").value =
                    document.getElementById("log").value + '\n' + nickname + " : " + body[id];
            }
        }
    }
    response_msg(event) {
        this.feed(event);
    }
    init() { }
}
__decorate([
    event_(0),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ChatClient.prototype, "show_msg", null);
__decorate([
    event_(1),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ChatClient.prototype, "response_msg", null);
