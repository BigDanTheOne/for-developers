import { BClient} from './game-net-framework-client/src/game-client/client.js'
import { IGameEvent } from './game-net-framework-client/src/common/events.js'
import {event_} from './game-net-framework-client/src/common/decorator.js'

export class ChatClient extends BClient {

    constructor(id : string) {
        super(id)
    }

    @event_(0)
    show_msg(event: IGameEvent): void {
        let body = JSON.parse(event.Body)
        for (let id in body) {
            if (id == "me") {
                (<HTMLInputElement>document.getElementById("log")).value =
                    (<HTMLInputElement>document.getElementById("log")).value + '\n' + id + " : " + body[id];
            } else {
                let nickname
                for (let player of this.players) {
                    if (player.Id == id) {
                        nickname = player.Name
                        break
                    }
                }
                (<HTMLInputElement>document.getElementById("log")).value =
                    (<HTMLInputElement>document.getElementById("log")).value + '\n' + nickname + " : " + body[id];
            }
        }
    }

    @event_(1)
    response_msg(event: IGameEvent){
        this.feed(event)
    }

    init(): void {}

}
