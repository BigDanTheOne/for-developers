import { BServer} from './game-net-framework-server/src/game-server/server'
import {IGameEvent} from "./game-net-framework-server/src/common/events"
import {event_} from "./game-net-framework-server/src/common/decorator"
import {serve} from "./game-net-framework-server/src/game-server/service";


class ChatServer extends BServer {

    constructor() {
        super()
    }

    @event_(1)
    response_msg(event: IGameEvent) {
        let new_event = {EventType : 0, Broadcast : false, Body : ""}
        let new_body = new Object(null)
        let obj_body = JSON.parse(event.Body)
        for (let player of this.players) {
            for (let k in obj_body) {
                if (k == player.id) {
                    new_body[player.id] = JSON.stringify(Object.fromEntries(new Map().set("me", obj_body[k])))
                } else {
                    new_body[player.id] = JSON.stringify(Object.fromEntries(new Map().set(k, obj_body[k])))
                }
            }
        }
        new_event.Body = JSON.stringify(new_body)
        this.feed(new_event)
    }

    init(): void {

    }

}

serve("localhost:3000", ChatServer)