"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./game-net-framework-server/src/game-server/server");
const decorator_1 = require("./game-net-framework-server/src/common/decorator");
const service_1 = require("./game-net-framework-server/src/game-server/service");
class ChatServer extends server_1.BServer {
    constructor() {
        super();
    }
    response_msg(event) {
        let new_event = { EventType: 0, Broadcast: false, Body: "" };
        let new_body = new Object(null);
        let obj_body = JSON.parse(event.Body);
        console.log(event.Body);
        console.log(this.players);
        for (let player of this.players) {
            console.log(obj_body);
            for (let k in obj_body) {
                if (k == player.id) {
                    new_body[player.id] = JSON.stringify(Object.fromEntries(new Map().set("me", obj_body[k])));
                }
                else {
                    new_body[player.id] = JSON.stringify(Object.fromEntries(new Map().set(k, obj_body[k])));
                }
            }
        }
        console.log("new_body: ", new_body);
        new_event.Body = JSON.stringify(new_body);
        console.log("received: ", event, " => ", new_event);
        this.feed(new_event);
    }
    init() {
    }
}
__decorate([
    (0, decorator_1.event_)(1)
], ChatServer.prototype, "response_msg", null);
(0, service_1.serve)("localhost:3000", ChatServer);
