// package: trip.pb
// file: trip.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as trip_pb from "ChatGameServer/game-net-framework-server/proto/trip_pb";

interface IGameFactoryService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    newGameSession: IGameFactoryService_INewGameSession;
}

interface IGameFactoryService_INewGameSession extends grpc.MethodDefinition<trip_pb.GRPCClientMessage, trip_pb.GRPCServerMessage> {
    path: string; // "/trip.pb.GameFactory/NewGameSession"
    requestStream: true;
    responseStream: true;
    requestSerialize: grpc.serialize<trip_pb.GRPCClientMessage>;
    requestDeserialize: grpc.deserialize<trip_pb.GRPCClientMessage>;
    responseSerialize: grpc.serialize<trip_pb.GRPCServerMessage>;
    responseDeserialize: grpc.deserialize<trip_pb.GRPCServerMessage>;
}

export const GameFactoryService: IGameFactoryService;

export interface IGameFactoryServer {
    newGameSession: grpc.handleBidiStreamingCall<trip_pb.GRPCClientMessage, trip_pb.GRPCServerMessage>;
}

export interface IGameFactoryClient {
    newGameSession(): grpc.ClientDuplexStream<trip_pb.GRPCClientMessage, trip_pb.GRPCServerMessage>;
    newGameSession(options: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<trip_pb.GRPCClientMessage, trip_pb.GRPCServerMessage>;
    newGameSession(metadata: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<trip_pb.GRPCClientMessage, trip_pb.GRPCServerMessage>;
}

export class GameFactoryClient extends grpc.Client implements IGameFactoryClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public newGameSession(options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<trip_pb.GRPCClientMessage, trip_pb.GRPCServerMessage>;
    public newGameSession(metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<trip_pb.GRPCClientMessage, trip_pb.GRPCServerMessage>;
}
