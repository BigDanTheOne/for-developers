// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var trip_pb = require('./trip_pb.js');

function serialize_trip_pb_GRPCClientMessage(arg) {
  if (!(arg instanceof trip_pb.GRPCClientMessage)) {
    throw new Error('Expected argument of type trip.pb.GRPCClientMessage');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_trip_pb_GRPCClientMessage(buffer_arg) {
  return trip_pb.GRPCClientMessage.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_trip_pb_GRPCServerMessage(arg) {
  if (!(arg instanceof trip_pb.GRPCServerMessage)) {
    throw new Error('Expected argument of type trip.pb.GRPCServerMessage');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_trip_pb_GRPCServerMessage(buffer_arg) {
  return trip_pb.GRPCServerMessage.deserializeBinary(new Uint8Array(buffer_arg));
}


var GameFactoryService = exports.GameFactoryService = {
  newGameSession: {
    path: '/trip.pb.GameFactory/NewGameSession',
    requestStream: true,
    responseStream: true,
    requestType: trip_pb.GRPCClientMessage,
    responseType: trip_pb.GRPCServerMessage,
    requestSerialize: serialize_trip_pb_GRPCClientMessage,
    requestDeserialize: deserialize_trip_pb_GRPCClientMessage,
    responseSerialize: serialize_trip_pb_GRPCServerMessage,
    responseDeserialize: deserialize_trip_pb_GRPCServerMessage,
  },
};

exports.GameFactoryClient = grpc.makeGenericClientConstructor(GameFactoryService);
