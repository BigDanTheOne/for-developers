// package: trip.pb
// file: trip.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class NewGameSessionRequest extends jspb.Message { 
    getGameid(): string;
    setGameid(value: string): NewGameSessionRequest;

    clearUsersList(): void;
    getUsersList(): Array<User>;
    setUsersList(value: Array<User>): NewGameSessionRequest;
    addUsers(value?: User, index?: number): User;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): NewGameSessionRequest.AsObject;
    static toObject(includeInstance: boolean, msg: NewGameSessionRequest): NewGameSessionRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: NewGameSessionRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): NewGameSessionRequest;
    static deserializeBinaryFromReader(message: NewGameSessionRequest, reader: jspb.BinaryReader): NewGameSessionRequest;
}

export namespace NewGameSessionRequest {
    export type AsObject = {
        gameid: string,
        usersList: Array<User.AsObject>,
    }
}

export class User extends jspb.Message { 
    getName(): string;
    setName(value: string): User;

    getId(): string;
    setId(value: string): User;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): User.AsObject;
    static toObject(includeInstance: boolean, msg: User): User.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: User, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): User;
    static deserializeBinaryFromReader(message: User, reader: jspb.BinaryReader): User;
}

export namespace User {
    export type AsObject = {
        name: string,
        id: string,
    }
}

export class GameEvent extends jspb.Message { 
    getType(): number;
    setType(value: number): GameEvent;

    getBody(): string;
    setBody(value: string): GameEvent;

    getBroadcast(): boolean;
    setBroadcast(value: boolean): GameEvent;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GameEvent.AsObject;
    static toObject(includeInstance: boolean, msg: GameEvent): GameEvent.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GameEvent, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GameEvent;
    static deserializeBinaryFromReader(message: GameEvent, reader: jspb.BinaryReader): GameEvent;
}

export namespace GameEvent {
    export type AsObject = {
        type: number,
        body: string,
        broadcast: boolean,
    }
}

export class Scores extends jspb.Message { 

    getScoresMap(): jspb.Map<string, number>;
    clearScoresMap(): void;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Scores.AsObject;
    static toObject(includeInstance: boolean, msg: Scores): Scores.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Scores, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Scores;
    static deserializeBinaryFromReader(message: Scores, reader: jspb.BinaryReader): Scores;
}

export namespace Scores {
    export type AsObject = {

        scoresMap: Array<[string, number]>,
    }
}

export class GRPCClientMessage extends jspb.Message { 

    hasNewgamesessionrequest(): boolean;
    clearNewgamesessionrequest(): void;
    getNewgamesessionrequest(): NewGameSessionRequest | undefined;
    setNewgamesessionrequest(value?: NewGameSessionRequest): GRPCClientMessage;


    hasGameevent(): boolean;
    clearGameevent(): void;
    getGameevent(): GameEvent | undefined;
    setGameevent(value?: GameEvent): GRPCClientMessage;


    getBodyCase(): GRPCClientMessage.BodyCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GRPCClientMessage.AsObject;
    static toObject(includeInstance: boolean, msg: GRPCClientMessage): GRPCClientMessage.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GRPCClientMessage, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GRPCClientMessage;
    static deserializeBinaryFromReader(message: GRPCClientMessage, reader: jspb.BinaryReader): GRPCClientMessage;
}

export namespace GRPCClientMessage {
    export type AsObject = {
        newgamesessionrequest?: NewGameSessionRequest.AsObject,
        gameevent?: GameEvent.AsObject,
    }

    export enum BodyCase {
        BODY_NOT_SET = 0,
    
    NEWGAMESESSIONREQUEST = 1,

    GAMEEVENT = 2,

    }

}

export class GRPCServerMessage extends jspb.Message { 

    hasScores(): boolean;
    clearScores(): void;
    getScores(): Scores | undefined;
    setScores(value?: Scores): GRPCServerMessage;


    hasGameevent(): boolean;
    clearGameevent(): void;
    getGameevent(): GameEvent | undefined;
    setGameevent(value?: GameEvent): GRPCServerMessage;


    getBodyCase(): GRPCServerMessage.BodyCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GRPCServerMessage.AsObject;
    static toObject(includeInstance: boolean, msg: GRPCServerMessage): GRPCServerMessage.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GRPCServerMessage, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GRPCServerMessage;
    static deserializeBinaryFromReader(message: GRPCServerMessage, reader: jspb.BinaryReader): GRPCServerMessage;
}

export namespace GRPCServerMessage {
    export type AsObject = {
        scores?: Scores.AsObject,
        gameevent?: GameEvent.AsObject,
    }

    export enum BodyCase {
        BODY_NOT_SET = 0,
    
    SCORES = 1,

    GAMEEVENT = 2,

    }

}
