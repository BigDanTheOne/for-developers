export type UserID = string;

export class User {
    id: UserID;
    handle: string;

    constructor(id_ : UserID, handle_: string) {
        this.id = id_;
        this.handle = handle_;
    }
}