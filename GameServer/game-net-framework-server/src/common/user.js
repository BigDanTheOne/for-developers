"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
class User {
    constructor(id_, handle_) {
        this.id = id_;
        this.handle = handle_;
    }
}
exports.User = User;
