"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.event_ = void 0;
function event_(eventType, allow_overwriting = false) {
    return function (target, propertyKey, descriptor) {
        console.log("!!!!!!!!!!!!!!!!!");
        if (!target.__events__) {
            console.log("!!!!!!!!!!!!!!!!!");
            target.__events__ = new Map();
        }
        if (eventType < 0) {
            throw `Event type must be positive`;
        }
        if (target.__events__.get(eventType) === undefined) {
            target.__events__.set(eventType, descriptor.value);
        }
        else if (allow_overwriting == true) {
            target.__events__.set(eventType, descriptor.value);
        }
        else {
            throw `overwriting event type ${eventType} handler is forbidden
            (set allow_overwriting = true if you want to overwrite event type handler)`;
        }
        return descriptor;
    };
}
exports.event_ = event_;
