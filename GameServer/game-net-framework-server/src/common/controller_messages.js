"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ControllerMessagesFactory = exports.OnMessageControllerMessage = exports.OnErrorControllerMessage = exports.OnCloseControllerMessage = exports.OnOpenControllerMessage = exports.ControllerMessage = exports.ControllerMassageType = void 0;
var ControllerMassageType;
(function (ControllerMassageType) {
    ControllerMassageType[ControllerMassageType["OnErrorType"] = 0] = "OnErrorType";
    ControllerMassageType[ControllerMassageType["OnCloseType"] = 1] = "OnCloseType";
    ControllerMassageType[ControllerMassageType["OnOpenType"] = 2] = "OnOpenType";
    ControllerMassageType[ControllerMassageType["OnMessageType"] = 3] = "OnMessageType";
})(ControllerMassageType = exports.ControllerMassageType || (exports.ControllerMassageType = {}));
class ControllerMessage {
    constructor(id_, user_, input_message_id_) {
        this.id = id_;
        this.user = user_;
        this.input_message_id = input_message_id_;
    }
    serialize() {
        //
    }
}
exports.ControllerMessage = ControllerMessage;
class OnOpenControllerMessage extends ControllerMessage {
    constructor(id_, user_, input_message_id_, was_successful_) {
        super(id_, user_, input_message_id_);
        this.type = ControllerMassageType.OnOpenType;
        this.was_successful = was_successful_;
    }
}
exports.OnOpenControllerMessage = OnOpenControllerMessage;
class OnCloseControllerMessage extends ControllerMessage {
    constructor(id_, user_, input_message_id_, was_clean_) {
        super(id_, user_, input_message_id_);
        this.type = ControllerMassageType.OnCloseType;
        this.was_clean = was_clean_;
    }
}
exports.OnCloseControllerMessage = OnCloseControllerMessage;
class OnErrorControllerMessage extends ControllerMessage {
    constructor(id_, user_, input_message_id_, error_) {
        super(id_, user_, input_message_id_);
        this.type = ControllerMassageType.OnCloseType;
        this.error = error_;
    }
}
exports.OnErrorControllerMessage = OnErrorControllerMessage;
class OnMessageControllerMessage extends ControllerMessage {
    constructor(id_, user_, input_message_id_, message_) {
        super(id_, user_, input_message_id_);
        this.type = ControllerMassageType.OnCloseType;
        this.message = message_;
    }
}
exports.OnMessageControllerMessage = OnMessageControllerMessage;
class ControllerMessagesFactory {
    constructor() {
        this.counter = 0;
    }
    MakeControllerMessage(new_messege_type, user_, input_message_id_, custom) {
        // implement more complex logic of index incrementing
        //add assertions (may be they are included in typescript compiler)
        switch (new_messege_type) {
            case ControllerMassageType.OnOpenType: {
                return new OnOpenControllerMessage(String(this.counter++), user_, input_message_id_, custom);
            }
            case ControllerMassageType.OnCloseType: {
                return new OnCloseControllerMessage(String(this.counter++), user_, input_message_id_, custom);
            }
            case ControllerMassageType.OnErrorType: {
                return new OnErrorControllerMessage(String(this.counter++), user_, input_message_id_, custom);
            }
            case ControllerMassageType.OnMessageType: {
                return new OnMessageControllerMessage(String(this.counter++), user_, input_message_id_, custom);
            }
        }
    }
}
exports.ControllerMessagesFactory = ControllerMessagesFactory;
