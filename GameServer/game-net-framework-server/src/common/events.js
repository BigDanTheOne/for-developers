"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MakeIGameMessage = exports.IGameMessage = exports.Player = void 0;
const messages_js_1 = require("./messages.js");
class Player {
}
exports.Player = Player;
class IGameMessage {
}
exports.IGameMessage = IGameMessage;
function MakeIGameMessage(evnt) {
    console.log(evnt);
    return { EventType: evnt.EventType, Broadcast: evnt.Broadcast, Body: evnt.Body,
        MessageType: messages_js_1.MessageType.GameEventMessage };
}
exports.MakeIGameMessage = MakeIGameMessage;
