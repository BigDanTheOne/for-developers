import {EventType, EventHandler, IGameEvent, IGameMessage, MakeIGameMessage} from '../common/events'
import { User } from '../common/user'
import { Scores } from '../common/scores'
import {eventReceiver, gameCloser} from '../common/callbackes'



export abstract class BServer {
    __events__: Map<EventType, EventHandler>;
    players: User[];

    // callbacks
    event_cb: eventReceiver;
    closer_cb: gameCloser;

    constructor() {}

    public setPlayers(players: User[]) {
        this.players = players;
    }

    public trigger(event: IGameEvent) {
        if (this.__events__ === undefined) {
            throw 'No tagged handlers';
        }


        if (!this.__events__.has(event.EventType)) {
            throw `Event tag ${event.EventType} not handled, ` + 'event = ' + event.Body + event.EventType
        }

        this.__events__.get(event.EventType).apply(this, [event]);
    }

    /** init is called once the game starts */
    abstract init(): void;
    /** end signals that the game has ended and returns scores */
    end(scores: Scores): void {
        this.closer_cb(scores);
    }

    /** feeds server events to client */
    protected feed(event: IGameEvent): void {
        // TODO: figure out how to run in background
        this.event_cb(event);
    }

    on(receiver: eventReceiver) {
        if (this.event_cb != undefined) {
            throw `Event handler already set: ${this.event_cb}`;
        }
        this.event_cb = receiver;
    }

    onClose(closer: gameCloser) {
        if (this.closer_cb != undefined) {
            throw `Closer already set: ${this.closer_cb}`;
        }
        this.closer_cb = closer;
    }
}

interface emptyConstructor {
    (): (BServer);
}


export function wireServer<SS extends BServer>
(eventCallback: eventReceiver,
 closerCallback: gameCloser,
 playes: User[],
 type: {new(): SS}): SS {
    var server = new type();

    server.setPlayers(playes);

    server.on(eventCallback);
    server.onClose(closerCallback);

    server.init();

    return server;
}
