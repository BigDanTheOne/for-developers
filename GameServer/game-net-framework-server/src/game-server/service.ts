// @ts-ignore
import { Server, ServerDuplexStream, ServerCredentials } from "grpc";
import { IGameFactoryServer, GameFactoryService } from '../../proto/trip_grpc_pb';
import {GameEvent, GRPCClientMessage, GRPCServerMessage} from '../../proto/trip_pb';
import { BServer, wireServer } from "./server"
import { EventType, EventHandler, IGameEvent } from '../common/events'
import { Scores } from '../common/scores'

export class GameFactoryServer<T extends BServer> implements IGameFactoryServer {
    factory: { new(): T};

    constructor(factory: { new(): T}) {
        this.factory = factory;
    }

    newGameSession(
        call: ServerDuplexStream<GRPCClientMessage, GRPCServerMessage>
    ) {
        let server: BServer;
        call.once("data", (data: GRPCClientMessage) => {
            console.log("received msg:", data.getNewgamesessionrequest().getUsersList());
            const users = data
                .getNewgamesessionrequest()
                .getUsersList()
                .map((u) => { return {id: u.getId(), handle: u.getName()}; });

            server = wireServer(
                (e: IGameEvent) => {
                    let msg = new GRPCServerMessage().setGameevent(
                        new GameEvent().setType(e.EventType)
                            .setBody(e.Body)
                            .setBroadcast(e.Broadcast)
                    );
                    console.log("sending msg:", msg.getGameevent().getBody())
                    call.write(msg);
                },
                (s: Scores) => {
                    // TODO: write scores
                    // call.write(s);
                },
                users,
                this.factory
            )
        })

        call.on("data", function (data: GRPCClientMessage) {
            if (data.hasGameevent()) {
                console.log("received msg:", data.getGameevent().getBody());
                server.trigger({ EventType: data.getGameevent().getType(), Body: data.getGameevent().getBody() }
                )
            }
        })
        call.on("error", function (err: Error) {
            console.log(err)
        })
        call.on("end",
            () => {
            console.log("end")
            })
    }
}


export function serve<S extends BServer>(addr: string, srv: { new(): S }) {
    const server = (new Server());

    // @ts-ignore
    server.addService(GameFactoryService, new GameFactoryServer(srv));
    server.bind(addr, ServerCredentials.createInsecure());
    server.start();
    console.log("GRPC Server on ", addr)
}

