"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.wireServer = exports.BServer = void 0;
class BServer {
    constructor() { }
    setPlayers(players) {
        this.players = players;
    }
    trigger(event) {
        if (this.__events__ === undefined) {
            throw 'No tagged handlers';
        }
        if (!this.__events__.has(event.EventType)) {
            throw `Event tag ${event.EventType} not handled, ` + 'event = ' + event.Body + event.EventType;
        }
        this.__events__.get(event.EventType).apply(this, [event]);
    }
    /** end signals that the game has ended and returns scores */
    end(scores) {
        this.closer_cb(scores);
    }
    /** feeds server events to client */
    feed(event) {
        // TODO: figure out how to run in background
        this.event_cb(event);
    }
    on(receiver) {
        if (this.event_cb != undefined) {
            throw `Event handler already set: ${this.event_cb}`;
        }
        this.event_cb = receiver;
    }
    onClose(closer) {
        if (this.closer_cb != undefined) {
            throw `Closer already set: ${this.closer_cb}`;
        }
        this.closer_cb = closer;
    }
}
exports.BServer = BServer;
function wireServer(eventCallback, closerCallback, playes, type) {
    var server = new type();
    server.setPlayers(playes);
    server.on(eventCallback);
    server.onClose(closerCallback);
    server.init();
    return server;
}
exports.wireServer = wireServer;
