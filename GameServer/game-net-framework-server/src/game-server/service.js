"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.serve = exports.GameFactoryServer = void 0;
// @ts-ignore
const grpc_1 = require("grpc");
const trip_grpc_pb_1 = require("../../proto/trip_grpc_pb");
const trip_pb_1 = require("../../proto/trip_pb");
const server_1 = require("./server");
class GameFactoryServer {
    constructor(factory) {
        this.factory = factory;
    }
    newGameSession(call) {
        let server;
        call.once("data", (data) => {
            console.log("received msg:", data.getNewgamesessionrequest().getUsersList());
            const users = data
                .getNewgamesessionrequest()
                .getUsersList()
                .map((u) => { return { id: u.getId(), handle: u.getName() }; });
            server = (0, server_1.wireServer)((e) => {
                let msg = new trip_pb_1.GRPCServerMessage().setGameevent(new trip_pb_1.GameEvent().setType(e.EventType)
                    .setBody(e.Body)
                    .setBroadcast(e.Broadcast));
                console.log("sending msg:", msg.getGameevent().getBody())
                call.write(msg);
            }, (s) => {
                // TODO: write scores
                // call.write(s);
            }, users, this.factory);
        });
        call.on("data", function (data) {
            if (data.hasGameevent()) {
                console.log("received msg:", data.getGameevent().getBody());
                server.trigger({ EventType: data.getGameevent().getType(), Body: data.getGameevent().getBody() });
            }
        });
        call.on("error", function (err) {
            console.log(err);
        });
        call.on("end", () => {
            console.log("end");
        });
    }
}
exports.GameFactoryServer = GameFactoryServer;
function serve(addr, srv) {
    const server = (new grpc_1.Server());
    // @ts-ignore
    server.addService(trip_grpc_pb_1.GameFactoryService, new GameFactoryServer(srv));
    server.bind(addr, grpc_1.ServerCredentials.createInsecure());
    server.start();
    console.log("GRPC Server on ", addr);
}
exports.serve = serve;
